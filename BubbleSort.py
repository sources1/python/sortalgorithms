#!/usr/bin/env python
# coding: utf-8

# In[2]:


#Sort function
def bubbleSort(array):
    n = len(array)
    
    #Go through all positions
    for i in range(n):
        
        #Apply bubble sort
        for j in range(0,n-i-1):
            if array[j] > array[j+1]:
                array[j], array[j+1] = array[j+1], array[j]
                
#Start sort                
array = [190,1200,1,400,2,3,100,1000,6,800]
bubbleSort(array)

#Print results
print("The ordered array is: ", array)





