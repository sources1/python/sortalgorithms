#!/usr/bin/env python
# coding: utf-8

# In[1]:


#Sort function
def selectionSort(array):
    n = len(array)
    
    #Go through all positions
    for i in range(n):
        
        #Apply selection sort
        idxDes = i
        for j in range(i+1, n):
            if array[idxDes] > array[j]:
                idxDes = j
        
        array[i], array[idxDes] = array[idxDes], array[i]
                
#Start sort                
array = [190,1200,1,400,2,3,100,1000,6,800]
selectionSort(array)

#Print results
print("The ordered array is: ", array)


# In[ ]:




